-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 02 Avril 2020 à 00:31
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ledda_anthony_peche_info1d_2020`
--
 --

-- Database: ledda_anthony_peche_info1d_2020

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists ledda_anthony_peche_info1d_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS ledda_anthony_peche_info1d_2020;

-- Utilisation de cette base de donnée

USE ledda_anthony_peche_info1d_2020; 
-- --------------------------------------------------------

--
-- Structure de la table `t_equipement`
--

CREATE TABLE `t_equipement` (
  `id_Equipement` int(11) NOT NULL,
  `NomEquipement` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_equipement`
--

INSERT INTO `t_equipement` (`id_Equipement`, `NomEquipement`) VALUES
(1, 'Equipement de Guido'),
(2, 'Equipement de Sewards'),
(3, 'Equipement de Cready'),
(4, 'Equipement de Lorkings'),
(5, 'Equipement de Fearfull'),
(6, 'Equipement de Aldren'),
(7, 'Equipement de Banke'),
(8, 'Equipement de Roggers'),
(9, 'Equipement de Scarsbrick'),
(10, 'Equipement de Ragsdall'),
(11, 'Equipement de Sobey'),
(12, 'Equipement de Goldney'),
(13, 'Equipement de Neilus'),
(14, 'Equipement de Kiera'),
(15, 'Equipement de Birden');

-- --------------------------------------------------------

--
-- Structure de la table `t_equip_a_mat`
--

CREATE TABLE `t_equip_a_mat` (
  `id_EquipAMat` int(11) NOT NULL,
  `fk_Equipement` int(11) NOT NULL,
  `fk_Materiel` int(11) NOT NULL,
  `DateMateriel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_equip_a_mat`
--

INSERT INTO `t_equip_a_mat` (`id_EquipAMat`, `fk_Equipement`, `fk_Materiel`, `DateMateriel`) VALUES
(1, 3, 3, '2020-02-28 15:51:45'),
(2, 2, 2, '2020-02-28 15:51:45'),
(3, 5, 5, '2020-02-28 15:51:45'),
(4, 3, 3, '2020-02-28 15:51:45'),
(5, 4, 4, '2020-02-28 15:51:45');

-- --------------------------------------------------------

--
-- Structure de la table `t_especes`
--

CREATE TABLE `t_especes` (
  `id_Espece` int(11) NOT NULL,
  `ThumbnailEspeceLink` text,
  `NomEspece` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_especes`
--

INSERT INTO `t_especes` (`id_Espece`, `ThumbnailEspeceLink`, `NomEspece`) VALUES
(1, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Perche.jpg', 'Perche'),
(2, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Brochet.jpg', 'Grand brochet'),
(3, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Truite_Brune.jpg', 'Truite de lac (fario)'),
(4, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Omble_Chevalier.jpg', 'Omble chevalier'),
(5, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Chevaine.jpg', 'Chevaine'),
(6, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Truite_arc-en-ciel.jpg', 'Truite arc-en-ciel'),
(7, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Lotte.jpg', 'Lotte'),
(8, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Silure.jpg', 'Silure'),
(9, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Sandre.jpg', 'Sandre'),
(10, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Barbeau.jpg', 'Barbeau commun'),
(11, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Coregone_Lavaret.jpg', 'Coregone lavaret'),
(12, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Anguille.jpg', 'Anguille'),
(13, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Rotengle.jpg', 'Rotengle'),
(14, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Tanche.jpg', 'Tanche'),
(15, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Carpe.jpg', 'Carpe commune'),
(16, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Gardon.jpg', 'Gardon (vengeron)'),
(17, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Breme.jpg', 'Brème commune'),
(18, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_ESPECE\\Palee.jpg', 'Palée'),
(19, '', 'Autre'),
(20, NULL, ''),
(21, NULL, ''),
(22, NULL, ''),
(23, NULL, ''),
(24, NULL, ''),
(25, NULL, ''),
(26, NULL, ''),
(27, NULL, ''),
(28, NULL, ''),
(29, NULL, ''),
(30, NULL, ''),
(31, NULL, ''),
(32, NULL, '');

-- --------------------------------------------------------

--
-- Structure de la table `t_localisations`
--

CREATE TABLE `t_localisations` (
  `id_Localisation` int(11) NOT NULL,
  `GpsLatLocalisation` float NOT NULL COMMENT 'Position GPS latitude',
  `GpsLonLocalisation` float NOT NULL COMMENT 'Position GPS longitude',
  `NomLieuLocalisation` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_localisations`
--

INSERT INTO `t_localisations` (`id_Localisation`, `GpsLatLocalisation`, `GpsLonLocalisation`, `NomLieuLocalisation`) VALUES
(1, 46.8776, 6.85412, 'Lac de Neuchatel'),
(2, 46.5002, 6.60149, 'Lac Leman'),
(3, 46.7648, 6.61486, 'La Thielle'),
(4, 46.9169, 7.06221, 'Lac de Morat');

-- --------------------------------------------------------

--
-- Structure de la table `t_mails`
--

CREATE TABLE `t_mails` (
  `id_Mail` int(11) NOT NULL,
  `NomMail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_mails`
--

INSERT INTO `t_mails` (`id_Mail`, `NomMail`) VALUES
(1, 'jeanpierrepol@gmail.com'),
(2, 'email@update.com'),
(3, 'ldolling2@prnewswire.com'),
(4, 'emacaless3@cbslocal.com'),
(5, 'chibrax@gmail.com'),
(6, 'rgolsworthy5@telegraph.co.uk'),
(8, 'azute7@netvibes.com'),
(9, 'egillmor8@amazon.de'),
(10, 'dgeerling9@vimeo.com'),
(11, 'ayacobsohna@naver.com'),
(12, 'gblockeyb@cocolog-nifty.com'),
(13, 'mcatherinec@netvibes.com'),
(14, 'sdanilchevd@newsvine.com'),
(15, 'mdillewaye@diigo.com'),
(16, 'test1@module104.com');

-- --------------------------------------------------------

--
-- Structure de la table `t_materiels`
--

CREATE TABLE `t_materiels` (
  `id_Materiel` int(11) NOT NULL,
  `fk_TypeMateriel` int(11) NOT NULL,
  `MarqueMateriel` varchar(25) NOT NULL,
  `ArticleMateriel` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_materiels`
--

INSERT INTO `t_materiels` (`id_Materiel`, `fk_TypeMateriel`, `MarqueMateriel`, `ArticleMateriel`) VALUES
(2, 4, '13 Fishing', 'Defy Silver'),
(3, 5, 'Abu Garcia', 'Abu Pro Max Spinning Reel'),
(4, 5, 'Dragon', 'Team Dragon Z Spinning Reel'),
(5, 1, 'Ace', 'Fat Flipper Small'),
(6, 5, 'Mitchell', 'Avocet RZ Reel'),
(7, 6, 'Berkley', 'Big Game');

-- --------------------------------------------------------

--
-- Structure de la table `t_meteo`
--

CREATE TABLE `t_meteo` (
  `id_Meteo` int(11) NOT NULL,
  `ConditionMeteo` varchar(20) NOT NULL,
  `TemperatureMeteo` varchar(10) NOT NULL,
  `VentMeteo` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_meteo`
--

INSERT INTO `t_meteo` (`id_Meteo`, `ConditionMeteo`, `TemperatureMeteo`, `VentMeteo`) VALUES
(1, 'Nuageux', '15°C', '6.00 km/h'),
(2, 'Nuageux', '6°C', '10.00 km/h'),
(3, 'Pluvieux', '4°C', '17.00 km/h');

-- --------------------------------------------------------

--
-- Structure de la table `t_methodes`
--

CREATE TABLE `t_methodes` (
  `id_Methode` int(11) NOT NULL,
  `NomMethode` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_methodes`
--

INSERT INTO `t_methodes` (`id_Methode`, `NomMethode`) VALUES
(1, 'Lancer'),
(2, 'Peche au Jerkbait'),
(3, 'Peche au coup'),
(4, 'Peche au harpon'),
(5, 'Peche de fond'),
(6, 'Peche sur glace'),
(7, 'Peche verticale'),
(8, 'Peche a la ligne dans la vague'),
(9, 'Peche a la ligne en mer'),
(10, 'Peche a la ligne libre'),
(11, 'Peche a la ligne a main'),
(12, 'Peche a la mouche'),
(13, 'Peche a la traine'),
(14, 'Peche a la turlutte'),
(15, 'Autres');

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `id_Personne` int(11) NOT NULL,
  `NomPers` varchar(35) NOT NULL,
  `PrenomPers` varchar(25) NOT NULL,
  `DateNaissPers` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personnes`
--

INSERT INTO `t_personnes` (`id_Personne`, `NomPers`, `PrenomPers`, `DateNaissPers`) VALUES
(1, 'Selle', 'Guido', '2007-12-24'),
(2, 'Ardella', 'Sewards', '1969-01-07'),
(3, 'Norrie', 'Cready', '1948-02-24'),
(4, 'Margette', 'Lorkings', '1961-07-17'),
(5, 'Morgun', 'Fearfull', '1990-09-24'),
(6, 'Mortie', 'Aldren', '1983-09-22'),
(7, 'Tripp', 'Banke', '1987-12-19'),
(8, 'Erhart', 'Roggers', '1963-02-18'),
(9, 'Cinda', 'Scarsbrick', '2011-10-05'),
(10, 'Ann', 'Ragsdall', '1995-03-23'),
(11, 'Kameko', 'Sobey', '1948-10-17'),
(12, 'Germana', 'Goldney', '1974-12-08'),
(13, 'Sheffield', 'Neilus', '1976-11-29'),
(14, 'Oswell', 'Kiera', '1957-01-13'),
(15, 'Lenna', 'Birden', '2001-09-09'),
(16, 'Cuck', 'Florian', '2001-01-02'),
(17, 'Javet', 'Penis', '2001-01-01'),
(18, 'Javet', 'Penis', '2001-01-01'),
(19, 'Javet', 'Penis', '2001-01-01'),
(20, 'Schopfer', 'Jean-Daniel', '1970-01-01'),
(21, 'Macouille', 'Olive', '3417-11-25'),
(22, 'Monkeyd', 'Luffy', '3417-11-25'),
(23, 'Shrek', 'Shrek', '2020-04-01');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_a_equip`
--

CREATE TABLE `t_pers_a_equip` (
  `id_PersAEquip` int(11) NOT NULL,
  `fk_Personne` int(11) NOT NULL,
  `fk_Equipement` int(11) NOT NULL,
  `DateEquip` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_pers_a_equip`
--

INSERT INTO `t_pers_a_equip` (`id_PersAEquip`, `fk_Personne`, `fk_Equipement`, `DateEquip`) VALUES
(1, 1, 1, '2020-02-28 14:21:13'),
(2, 2, 2, '2020-02-28 14:21:13'),
(3, 3, 3, '2020-02-28 14:21:13'),
(4, 4, 4, '2020-02-28 14:21:13'),
(5, 5, 5, '2020-02-28 14:21:13'),
(6, 6, 6, '2020-02-28 14:21:13'),
(7, 7, 7, '2020-02-28 14:21:13'),
(8, 8, 8, '2020-02-28 14:21:13'),
(9, 9, 9, '2020-02-28 14:21:13'),
(10, 10, 10, '2020-02-28 14:21:13'),
(11, 11, 11, '2020-02-28 14:21:13'),
(12, 12, 12, '2020-02-28 14:21:13'),
(13, 13, 13, '2020-02-28 14:21:13'),
(14, 14, 14, '2020-02-28 14:21:13'),
(15, 15, 15, '2020-02-28 14:21:13');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_a_mails`
--

CREATE TABLE `t_pers_a_mails` (
  `id_PersAMail` int(11) NOT NULL,
  `fk_Personne` int(11) NOT NULL,
  `fk_Mail` int(11) NOT NULL,
  `DateMail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_pers_a_mails`
--

INSERT INTO `t_pers_a_mails` (`id_PersAMail`, `fk_Personne`, `fk_Mail`, `DateMail`) VALUES
(1, 2, 3, '2020-02-28 14:59:41'),
(2, 4, 2, '2020-02-28 14:59:41'),
(3, 10, 10, '2020-02-28 14:59:41'),
(4, 14, 14, '2020-02-28 14:59:41'),
(5, 15, 15, '2020-02-28 14:59:41'),
(6, 9, 9, '2020-02-28 14:59:41'),
(7, 2, 6, '2020-02-28 14:59:41'),
(8, 3, 10, '2020-03-31 13:29:01'),
(9, 5, 3, '2020-04-01 22:51:15'),
(10, 4, 3, '2020-04-02 00:15:43');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_e_prise`
--

CREATE TABLE `t_pers_e_prise` (
  `id_PersEPrise` int(11) NOT NULL,
  `fk_Personne` int(11) NOT NULL,
  `fk_Prise` int(11) NOT NULL,
  `DatePersPrise` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_pers_e_prise`
--

INSERT INTO `t_pers_e_prise` (`id_PersEPrise`, `fk_Personne`, `fk_Prise`, `DatePersPrise`) VALUES
(1, 2, 1, '2020-03-13 16:33:45'),
(2, 5, 2, '2020-03-13 16:33:45'),
(3, 7, 3, '2020-03-13 16:33:45');

-- --------------------------------------------------------

--
-- Structure de la table `t_prises`
--

CREATE TABLE `t_prises` (
  `id_Prise` int(11) NOT NULL,
  `fk_Methode` int(11) NOT NULL,
  `fk_Especes` int(11) NOT NULL,
  `PhotoPriseLink` text,
  `PoidPrise` varchar(10) NOT NULL,
  `TaillePrise` varchar(10) NOT NULL,
  `DatePrise` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_prises`
--

INSERT INTO `t_prises` (`id_Prise`, `fk_Methode`, `fk_Especes`, `PhotoPriseLink`, `PoidPrise`, `TaillePrise`, `DatePrise`) VALUES
(1, 1, 2, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_PRISE\\brochet_macron.png', '10 kg', '113 cm', '2019-11-05 10:00:00'),
(2, 4, 3, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_PRISE\\truite_prise.jpg', '6 kg', '80 cm', '2020-03-11 10:00:00'),
(3, 1, 2, 'C:\\Users\\Anthony\\Documents\\module 104\\IMG_PRISE\\brochet_prise2.jpg', '9 kg', '80 cm', '2020-02-07 06:00:00'),
(4, 1, 1, NULL, '4 kg', '85 cm', '2020-01-01 10:00:00'),
(5, 1, 1, NULL, '5 kg', '90 cm', '2020-01-01 10:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `t_prise_a_local`
--

CREATE TABLE `t_prise_a_local` (
  `id_PriseALocal` int(11) NOT NULL,
  `fk_Prise` int(11) NOT NULL,
  `fk_Localisation` int(11) NOT NULL,
  `DateLocalisation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_prise_a_local`
--

INSERT INTO `t_prise_a_local` (`id_PriseALocal`, `fk_Prise`, `fk_Localisation`, `DateLocalisation`) VALUES
(1, 1, 1, '2020-03-13 16:34:25'),
(2, 2, 2, '2020-03-13 16:34:25'),
(3, 3, 3, '2020-03-13 16:34:25');

-- --------------------------------------------------------

--
-- Structure de la table `t_prise_a_meteo`
--

CREATE TABLE `t_prise_a_meteo` (
  `id_PriseAMeteo` int(11) NOT NULL,
  `fk_Prise` int(11) NOT NULL,
  `fk_Meteo` int(11) NOT NULL,
  `DateMeteo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_typemateriel`
--

CREATE TABLE `t_typemateriel` (
  `id_TypeMateriel` int(11) NOT NULL,
  `NomTypeMateriel` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_typemateriel`
--

INSERT INTO `t_typemateriel` (`id_TypeMateriel`, `NomTypeMateriel`) VALUES
(1, 'Appats et leurres'),
(2, 'Natural baits'),
(3, 'Flies'),
(4, 'Cannes'),
(5, 'Moulinets'),
(6, 'Combos'),
(7, 'Lignes'),
(8, 'Accessoires'),
(9, 'Autres');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_equipement`
--
ALTER TABLE `t_equipement`
  ADD PRIMARY KEY (`id_Equipement`);

--
-- Index pour la table `t_equip_a_mat`
--
ALTER TABLE `t_equip_a_mat`
  ADD PRIMARY KEY (`id_EquipAMat`),
  ADD KEY `fk_Equipement` (`fk_Equipement`),
  ADD KEY `fk_Materiel` (`fk_Materiel`);

--
-- Index pour la table `t_especes`
--
ALTER TABLE `t_especes`
  ADD PRIMARY KEY (`id_Espece`);

--
-- Index pour la table `t_localisations`
--
ALTER TABLE `t_localisations`
  ADD PRIMARY KEY (`id_Localisation`);

--
-- Index pour la table `t_mails`
--
ALTER TABLE `t_mails`
  ADD PRIMARY KEY (`id_Mail`);

--
-- Index pour la table `t_materiels`
--
ALTER TABLE `t_materiels`
  ADD PRIMARY KEY (`id_Materiel`),
  ADD KEY `fk_TypeMateriel` (`fk_TypeMateriel`);

--
-- Index pour la table `t_meteo`
--
ALTER TABLE `t_meteo`
  ADD PRIMARY KEY (`id_Meteo`);

--
-- Index pour la table `t_methodes`
--
ALTER TABLE `t_methodes`
  ADD PRIMARY KEY (`id_Methode`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`id_Personne`);

--
-- Index pour la table `t_pers_a_equip`
--
ALTER TABLE `t_pers_a_equip`
  ADD PRIMARY KEY (`id_PersAEquip`),
  ADD KEY `fk_Personne` (`fk_Personne`),
  ADD KEY `fk_Equipement` (`fk_Equipement`);

--
-- Index pour la table `t_pers_a_mails`
--
ALTER TABLE `t_pers_a_mails`
  ADD PRIMARY KEY (`id_PersAMail`),
  ADD KEY `fk_Personne` (`fk_Personne`),
  ADD KEY `fk_Mail` (`fk_Mail`);

--
-- Index pour la table `t_pers_e_prise`
--
ALTER TABLE `t_pers_e_prise`
  ADD PRIMARY KEY (`id_PersEPrise`),
  ADD KEY `fk_Prise` (`fk_Prise`),
  ADD KEY `fk_Personne` (`fk_Personne`);

--
-- Index pour la table `t_prises`
--
ALTER TABLE `t_prises`
  ADD PRIMARY KEY (`id_Prise`),
  ADD KEY `fk_Methode` (`fk_Methode`),
  ADD KEY `fk_Especes` (`fk_Especes`);

--
-- Index pour la table `t_prise_a_local`
--
ALTER TABLE `t_prise_a_local`
  ADD PRIMARY KEY (`id_PriseALocal`),
  ADD KEY `fk_Prise` (`fk_Prise`),
  ADD KEY `fk_Localisation` (`fk_Localisation`);

--
-- Index pour la table `t_prise_a_meteo`
--
ALTER TABLE `t_prise_a_meteo`
  ADD PRIMARY KEY (`id_PriseAMeteo`),
  ADD KEY `fk_Prise` (`fk_Prise`),
  ADD KEY `fk_Meteo` (`fk_Meteo`);

--
-- Index pour la table `t_typemateriel`
--
ALTER TABLE `t_typemateriel`
  ADD PRIMARY KEY (`id_TypeMateriel`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_equipement`
--
ALTER TABLE `t_equipement`
  MODIFY `id_Equipement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_equip_a_mat`
--
ALTER TABLE `t_equip_a_mat`
  MODIFY `id_EquipAMat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `t_especes`
--
ALTER TABLE `t_especes`
  MODIFY `id_Espece` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pour la table `t_localisations`
--
ALTER TABLE `t_localisations`
  MODIFY `id_Localisation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_mails`
--
ALTER TABLE `t_mails`
  MODIFY `id_Mail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `t_materiels`
--
ALTER TABLE `t_materiels`
  MODIFY `id_Materiel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_meteo`
--
ALTER TABLE `t_meteo`
  MODIFY `id_Meteo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_methodes`
--
ALTER TABLE `t_methodes`
  MODIFY `id_Methode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `id_Personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `t_pers_a_equip`
--
ALTER TABLE `t_pers_a_equip`
  MODIFY `id_PersAEquip` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_pers_a_mails`
--
ALTER TABLE `t_pers_a_mails`
  MODIFY `id_PersAMail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `t_pers_e_prise`
--
ALTER TABLE `t_pers_e_prise`
  MODIFY `id_PersEPrise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_prises`
--
ALTER TABLE `t_prises`
  MODIFY `id_Prise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `t_prise_a_local`
--
ALTER TABLE `t_prise_a_local`
  MODIFY `id_PriseALocal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_prise_a_meteo`
--
ALTER TABLE `t_prise_a_meteo`
  MODIFY `id_PriseAMeteo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_typemateriel`
--
ALTER TABLE `t_typemateriel`
  MODIFY `id_TypeMateriel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_equip_a_mat`
--
ALTER TABLE `t_equip_a_mat`
  ADD CONSTRAINT `t_equip_a_mat_ibfk_1` FOREIGN KEY (`fk_Equipement`) REFERENCES `t_equipement` (`id_Equipement`),
  ADD CONSTRAINT `t_equip_a_mat_ibfk_2` FOREIGN KEY (`fk_Materiel`) REFERENCES `t_materiels` (`id_Materiel`);

--
-- Contraintes pour la table `t_materiels`
--
ALTER TABLE `t_materiels`
  ADD CONSTRAINT `t_materiels_ibfk_1` FOREIGN KEY (`fk_TypeMateriel`) REFERENCES `t_typemateriel` (`id_TypeMateriel`);

--
-- Contraintes pour la table `t_pers_a_equip`
--
ALTER TABLE `t_pers_a_equip`
  ADD CONSTRAINT `t_pers_a_equip_ibfk_1` FOREIGN KEY (`fk_Personne`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_a_equip_ibfk_2` FOREIGN KEY (`fk_Equipement`) REFERENCES `t_equipement` (`id_Equipement`);

--
-- Contraintes pour la table `t_pers_a_mails`
--
ALTER TABLE `t_pers_a_mails`
  ADD CONSTRAINT `t_pers_a_mails_ibfk_1` FOREIGN KEY (`fk_Personne`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_a_mails_ibfk_2` FOREIGN KEY (`fk_Mail`) REFERENCES `t_mails` (`id_Mail`);

--
-- Contraintes pour la table `t_pers_e_prise`
--
ALTER TABLE `t_pers_e_prise`
  ADD CONSTRAINT `t_pers_e_prise_ibfk_1` FOREIGN KEY (`fk_Personne`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_e_prise_ibfk_2` FOREIGN KEY (`fk_Prise`) REFERENCES `t_prises` (`id_Prise`);

--
-- Contraintes pour la table `t_prises`
--
ALTER TABLE `t_prises`
  ADD CONSTRAINT `t_prises_ibfk_1` FOREIGN KEY (`fk_Methode`) REFERENCES `t_methodes` (`id_Methode`),
  ADD CONSTRAINT `t_prises_ibfk_2` FOREIGN KEY (`fk_Especes`) REFERENCES `t_especes` (`id_Espece`);

--
-- Contraintes pour la table `t_prise_a_local`
--
ALTER TABLE `t_prise_a_local`
  ADD CONSTRAINT `t_prise_a_local_ibfk_1` FOREIGN KEY (`fk_Prise`) REFERENCES `t_prises` (`id_Prise`),
  ADD CONSTRAINT `t_prise_a_local_ibfk_2` FOREIGN KEY (`fk_Localisation`) REFERENCES `t_localisations` (`id_Localisation`);

--
-- Contraintes pour la table `t_prise_a_meteo`
--
ALTER TABLE `t_prise_a_meteo`
  ADD CONSTRAINT `t_prise_a_meteo_ibfk_1` FOREIGN KEY (`fk_Prise`) REFERENCES `t_prises` (`id_Prise`),
  ADD CONSTRAINT `t_prise_a_meteo_ibfk_2` FOREIGN KEY (`fk_Meteo`) REFERENCES `t_meteo` (`id_Meteo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
